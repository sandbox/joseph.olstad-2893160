---
# This is a drush make file for a media_wysiwyg setup using the CKEditor
# module.
#
# Build a new d7 site using this recipe with this drush command:
# drush make /path/to/this/file /path/to/destination/folder
# HOWEVER, if you want to build it without core, you will have to run this command
# drush make /path/to/this/file --no-core /path/to/destination/folder
#
# After downloading and moving files into place, here are the manual steps
# needed to set everything up:
#
# 1. If Drupal is not already installed, install Drupal with the standard
#    profile: drush site-install
# 2. Enable these modules:
#    - media_ckeditor
#    - token
#    - media_oembed
# 3. At /admin/config/content/ckeditor/editg change the value for
#    "Path to the CKEditor plugins directory" to: %l/ckeditor/plugins
# 4. Go to a text format edit page (such as /admin/config/content/formats/my_format),
#    then make sure this checkbox is checked:
#    - "Convert Media tags to markup"
#    make sure that - "Convert URLs into links" is at the bottom, or else disabled.
#    If you do not have these ordered correct Drupal will render codes instead of images.
# 5. Go to the CKEditor profile edit page for the text format (such as
#    /admin/config/content/ckeditor/edit/MyFormat) and make sure these
#    checkboxes are checked under the "Editor Appearance" section:
#    - "Plugin for embedding files using Media CKEditor"
#    - "Plugin file: lineutils"
#    - "Plugin file: widget"
#    - "Plugin file: widgetselection"
# 6. Set your MyFormat as the default text format:
#     - go to /admin/config/content/formats drag your MyFormat to the top
#     - click save
# 7. Disable the ACF in the "Advanced Content Filter" section

api: 2
core: 7.56

# Necessary modules.
projects:
  drupal:
    version: "7.56"
    patch:
    - "https://www.drupal.org/files/issues/aria-describedby_2.patch"
    - "https://www.drupal.org/files/issues/menu-access_unpublished-nodes_460408-157.patch"
    - "https://www.drupal.org/files/issues/install-redirect-on-empty-database-728702-36.patch"
    - "https://www.drupal.org/files/drupal-865536-204.patch"
    - "https://www.drupal.org/files/911354-drupal-profile-85.patch"
    - "https://www.drupal.org/files/issues/998898-63chars-identifier-limit-nomd5-D7-75.patch"
    - "https://www.drupal.org/files/drupal-1470656-14.patch"
    - "https://www.drupal.org/files/drupal-1710656-3-skip-hidden-menu-items-D7.patch"
    - "https://www.drupal.org/files/drupal7-allow_change_system-requirements-1772316-18.patch"
    - "https://www.drupal.org/files/issues/2383823-check_name_empty-26.patch"
    - "https://www.drupal.org/files/issues/core_allow_textfield_textarea_widgets-2479895-8.patch"
    - "https://www.drupal.org/files/issues/reset-flood-2880910-22.patch"
    - "https://www.drupal.org/files/issues/core-more_accurate_image_upload_text-1079116-144.patch"
    - "https://www.drupal.org/files/issues/drupal-n973436-113.patch"
    - "https://www.drupal.org/files/issues/drupal-n1081192-71.patch"
    - "https://www.drupal.org/files/issues/drupal-1978176-menu_load_objects-75.patch"
    - "https://www.drupal.org/files/issues/node_access_grants-static-cache-19.patch"
    - "https://www.drupal.org/files/issues/2789723-1.patch"
    - "https://www.drupal.org/files/issues/reordering_fails_with-1007746-157.patch"
    - "https://www.drupal.org/files/issues/change_breadcrumb_strings_into_array-2863108-11.patch"
    - "https://www.drupal.org/files/issues/autocomplete-security-optional-2749007-9.patch"
    - "https://www.drupal.org/files/issues/drupal-revision-revert-messes-up-field-translation-1992010-31_D7.patch"
    - "https://www.drupal.org/files/issues/drupal-502430-37.patch"
    - "https://www.drupal.org/files/issues/drupal-936316-28_0.patch"
    - "https://www.drupal.org/files/issues/2756297-21.patch"
    - "https://www.drupal.org/files/issues/d7_alter_js_settings_ajax-2171113-27.patch"
    - "https://www.drupal.org/files/issues/missing-modules-notice-2844716-5.patch"
    - "https://www.drupal.org/files/issues/edit_add_user_action-512042-54.patch"
    - "https://www.drupal.org/files/issues/494518-91.patch"
    - "https://www.drupal.org/files/issues/drupal-file_unmanaged_move_rename_where_possible-2752783-9.patch"
    - "https://www.drupal.org/files/issues/1440312-61.patch"
    - "https://www.drupal.org/files/issues/cleanup-files-1399846-316.patch"
    - "https://www.drupal.org/files/issues/drupal-2909714-3-drupal_get_query_array-urldecode.patch"
    - "https://www.drupal.org/files/issues/core-use_reply_to_instead_of_from-111702-149.patch"
    - "https://www.drupal.org/files/theme-settings-in-build-info-1862892-7.patch"
  drafty:
    version: "1.0-rc1"
    type: "module"
    subdir: "contrib"
    patch:
    - "https://www.drupal.org/files/issues/7x1x_drafty_publish_from_admin_content_page-2910310-10.patch"
  media:
    version: "2.14"
    type: "module"
    subdir: "contrib"
  media_ckeditor:
    version: "2.5"
    type: "module"
    subdir: "contrib"
  media_oembed:
    version: "2.7"
    type: "module"
    subdir: "contrib"
  entity:
    subdir: "contrib"
    download:
      type: "git"
      branch: "7.x-1.x"
      url: "https://git.drupal.org/project/entity.git"
      revision: "d50db77d"
    patch:
    - "https://www.drupal.org/files/issues/entity-1312374-42-fatal-error-if-missing-property-callback.patch"
    - "https://www.drupal.org/files/issues/2886570-Add_Entity_Help-7.patch"
    - "https://www.drupal.org/files/issues/entity-ctools-content-type-from-context-2020325-38.patch"
  entity_translation_unified_form:
    subdir: "contrib"
    version: "1.0-beta1"
  entity_translation:
    subdir: "contrib"
    version: "1.0-beta7"
    patch:
    - "https://www.drupal.org/files/issues/entity_translation-translation_status_confusing-2908096-5.patch"
    - "https://www.drupal.org/files/issues/static_cache_for-2557429-17.patch"
    - "https://www.drupal.org/files/issues/entity_translation-2734295-4.patch"
  i18n:
    version: "1.22"
    subdir: "contrib"
  date:
    version: "2.10"
    subdir: "contrib"
  jquery_update:
    version: "3.0-alpha5"
    subdir: "contrib"
  views:
    version: "3.18"
    subdir: "contrib"
    patch:
    - "https://www.drupal.org/files/issues/views_1189550_escape_rss_feed_title.patch"
    - "https://www.drupal.org/files/issues/views-3.x-dev-issue_1331056-52.patch"
    - "https://www.drupal.org/files/views-exposed-sorts-2037469-1.patch"
    - "https://www.drupal.org/files/issues/2071607-29.patch"
    - "https://www.drupal.org/files/issues/replace_views_include-2653214-2.patch"
    - "https://www.drupal.org/files/issues/avoid_excess_t_calls-2653266-3.patch"
    - "https://www.drupal.org/files/issues/reduce_the_calls_to_the-2760405-2.patch"
    - "https://www.drupal.org/files/issues/get_option_micro-2760419-2.patch"
    - "https://www.drupal.org/files/issues/views-and_missing_parenthesis-2908538-2-D7.patch"
  i18nviews:
    version: "3.0-alpha1"
    subdir: "contrib"
    patch:
    - "http://drupal.org/files/issues/transformed-contextual-filter-fix-178832-10.patch"
    - "https://www.drupal.org/files/issues/i18nviews-sort-by-localized-name-1645304-13.patch"
    - "https://www.drupal.org/files/issues/i18nviews-add-readme-2849557-2.patch"
    - "https://www.drupal.org/files/issues/i18nviews-2245917-1-export-translatables-v1.patch"
    - "https://www.drupal.org/files/issues/i18nviews_contextual_link-2018127-2.patch"
  ckeditor_link:
    subdir: "contrib"
    download:
      type: "git"
      branch: "7.x-2.x"
      url: "https://git.drupal.org/project/ckeditor_link.git"
      revision: "495b5e2"
    patch:
    - "https://www.drupal.org/files/issues/ckeditor_link-entity-translation-1875720-42-D7.patch"
    - "https://www.drupal.org/files/issues/ckeditor_link-allow-args-query-hash-1491750-36.patch"
    - "https://www.drupal.org/files/issues/fix_multilingual-2471559-4.patch"
  forum_access:
    version: "1.3"
    type: "module"
    subdir: "contrib"
  admin_menu:
    version: "3.x"
    type: "module"
    subdir: "contrib"
  advanced_forum:
    version: "2.6"
    type: "module"
    subdir: "contrib"
  advpoll:
    subdir: "contrib"
    download:
      type: "git"
      branch: "7.x-3.x"
      url: "https://git.drupal.org/project/advpoll.git"
      revision: "3195f1a84"
    patch:
    - "https://www.drupal.org/files/issues/advpoll-xss_issue-2121459-7.patch"
    - "https://www.drupal.org/files/issues/advpoll-fix_incorrects_items_at_votes_page-2827161-4.patch"
    - "https://www.drupal.org/files/issues/advpoll-views-integration-1540674-14.patch"
  variable:
    version: "2.5"
    subdir: "contrib"
    patch:
    - "https://www.drupal.org/files/issues/2799811-2.patch"
    - "https://www.drupal.org/files/issues/variable-fix_ajax_variable_realm-2141531-10-D7.patch"
    - "https://www.drupal.org/files/issues/incorrect_variable_get_value-2432801-2.patch"
    - "https://www.drupal.org/files/issues/typo_in_hook_variable_info-2627438-2.patch"
    - "https://www.drupal.org/files/issues/2574061-2.patch"
    - "https://www.drupal.org/files/issues/2574689-3.variable.export-code-indentation.patch"
    - "https://www.drupal.org/files/issues/2576245.variable.language-export-broken.patch"
    - "https://www.drupal.org/files/issues/variable.realm-variable-config-form-missing-a-title.patch"
  l10n_client:
    subdir: "contrib"
    download:
      type: "git"
      branch: "7.x-1.x"
      url: "https://git.drupal.org/project/l10n_client.git"
      revision: "6381b6483"
  votingapi:
    version: "2.12"
    subdir: "contrib"
    patch:
    - "https://www.drupal.org/files/issues/updating_voting_api_default_views-2679585-3.patch"
    - "https://www.drupal.org/files/issues/2844706-Add_VotingAPI_Help-2.patch"
    - "https://www.drupal.org/files/issues/add_migration_support-2697163-3.patch"
    - "https://www.drupal.org/files/issues/2609136-2.patch"
    - "https://www.drupal.org/files/issues/votingapi-d7_batch_recalculate-382866-69.patch"
  acl:
    version: "1.1"
    type: "module"
    subdir: "contrib"
    patch:
    - "https://www.drupal.org/files/issues/acl-add-acl_remove_all_users-function-2580207-12-d7.patch"
  l10n_update:
    version: "2.2"
    type: "module"
    subdir: "contrib"
  stockapi:
    version: "1.x"
    subdir: "contrib"
  stock:
    version: "1.x"
    subdir: "contrib"
  title:
    version: "1.0-alpha9"
    subdir: "contrib"
    patch:
    - "https://www.drupal.org/files/issues/title-text_formats-2757739-23.patch"
    - "https://www.drupal.org/files/issues/1811116-22.patch"
    - "https://www.drupal.org/files/issues/title-unnecessary_sync-2542826-01.patch"
    - "https://www.drupal.org/files/issues/remove_callback-2293347-7.patch"
    - "https://www.drupal.org/files/issues/title-2531774-1.patch"
    - "https://www.drupal.org/files/issues/property_as_fallback-linkit-2637638-1.patch"
    - "https://www.drupal.org/files/issues/title-fix_page_title-2505311-7.patch"
    - "https://www.drupal.org/files/issues/title-fix_comment_for_hook_function_for_views-2827746-2.patch"
    - "https://www.drupal.org/files/issues/remove-extra-comments_2771229.patch"
    - "https://www.drupal.org/files/1801242-title-upgrade.patch"
  webform:
    subdir: "contrib"
    download:
      type: "git"
      branch: "7.x-4.x"
      url: "https://git.drupal.org/project/webform.git"
      revision: "03f5f64bb"
  webform_localization:
    version: "4.10"
    subdir: "contrib"
  workbench_moderation:
    subdir: "contrib"
    download:
      type: "git"
      branch: "7.x-3.x"
      url: "https://git.drupal.org/project/workbench_moderation.git"
      revision: "3fcb1a66"
    patch:
    - "https://www.drupal.org/files/issues/upgrade_from_1x3_to_3x_fails-2428371-51.patch"
  access_unpublished:
    version: "1.03"
    type: "module"
    subdir: "contrib"
  chain_menu_access:
    version: "2.0"
    type: "module"
    subdir: "contrib"
  honeypot:
    version: "1.22"
    type: "module"
    subdir: "contrib"
    patch:
    - "https://www.drupal.org/files/issues/honeypot-permissions_check-2839775-6.patch"
    - "https://www.drupal.org/files/issues/honeypot-2845272-views-exposed-form-support.patch"
    - "https://www.drupal.org/files/issues/ajax_file_upload_form-2396193-7.patch"
  ctools:
    subdir: "contrib"
    download:
      type: "git"
      branch: "7.x-1.x"
      url: "https://git.drupal.org/project/ctools.git"
      revision: "166eac2"
    patch:
    - "https://www.drupal.org/files/issues/ctools-2399313-1-Relationship-optional-context.patch"
    - "https://www.drupal.org/files/issues/ctools-views-content-custom-url-2401635-02.patch"
    - "https://www.drupal.org/files/issues/2422123-node_view-29.patch"
    - "https://www.drupal.org/files/issues/ctools-2905885-3.patch"
    - "https://www.drupal.org/files/issues/ctools-jquery_compatibility-2787045-27.patch"
  file_entity:
    version: "2.12"
    type: "module"
    subdir: "contrib"
    patch:
    - "https://www.drupal.org/files/issues/allow_selection_of-2000934-45.patch"
  token:
    version: "1.7"
    type: "module"
    subdir: "contrib"
  ckeditor:
    version: "1.18"
    type: "module"
    subdir: "contrib"
  boost:
    version: ""
    subdir: "contrib"
    patch:
    - "https://www.drupal.org/files/boost-add-vary-cookie-directive.patch"
    - "https://www.drupal.org/files/issues/boost-n2259987-3.patch"
    - "https://www.drupal.org/files/issues/boost-DRUPAL_UID_expiration-2571007-1.patch"
    - "https://www.drupal.org/files/boost-cron-detection-1829832-3.patch"
  navbar:
    version: "1.7"
    subdir: "contrib"
    patch:
    - "https://www.drupal.org/files/issues/navbar_link_language-2644930-3.patch"
    - "https://www.drupal.org/files/issues/navbar_modernizr-2377149-1.patch"
  admin_views:
    version:
    subdir: "contrib"
    patch:
    - "https://www.drupal.org/files/issues/2545750-7.patch"
    - "https://www.drupal.org/files/issues/admin_views-sort_criteria_is_not_working-2802533-4.patch"
    - "https://www.drupal.org/files/issues/admin_views-add-readme-2836511-5.patch"
    - "https://www.drupal.org/files/issues/admin_views-mollom-integration-2.patch"
  libraries:
    type: "module"
    subdir: "custom"
    download:
      type: "git"
      branch: "7.x-2.x"
      revision: "bc427ef0"
    patch:
    - "https://www.drupal.org/files/issues/base_theme_is_not-2815965-10.patch"
    - "https://www.drupal.org/files/issues/libraries-version-callback-reference-parameter-fix-2779591-11.patch"
    - "https://www.drupal.org/files/issues/libraries-override-capturing-group-for-version-2849058-1.patch"

# Optional modules.

# The following allows for bulk uploads. After installing, enable media_bulk_upload.
  plupload:
    version: "1.7"
    type: "module"
    subdir: "contrib"
  multiform:
    version: "1.4"
    type: "module"
    subdir: "contrib"

# Necessary themes.
  bootstrap:
    type: "theme"
    download:
      type: "git"
      branch: "7.x-3.x"
      revision: "85b09470a"
    patch:
    - "https://www.drupal.org/files/issues/add_drush_support_for-2261189-15.patch"

# Custom themes

# Custom modules
  boost_blast:
    type: "module"
    subdir: "custom"
    download:
      type: "git"
      branch: "7.x-1.x"
      url: "http://git.drupal.org/sandbox/joseph.olstad/2635830.git"
      revision: "e5f30e1633e"

# Necessary libraries.
libraries:
  ckeditor:
    download:
      type: "file"
      url: "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.7.1/ckeditor_4.7.1_full.zip"
  lineutils:
    download:
      type: "file"
      url: "http://download.ckeditor.com/lineutils/releases/lineutils_4.7.1.zip"
    destination: "libraries/ckeditor/plugins"
  widget:
    download:
      type: "file"
      url: "http://download.ckeditor.com/widget/releases/widget_4.7.1.zip"
    destination: "libraries/ckeditor/plugins"
  widgetselection:
    download:
      type: "file"
      url: "http://download.ckeditor.com/widgetselection/releases/widgetselection_4.7.1.zip"
    destination: "libraries/ckeditor/plugins"
  moono:
    download:
      type: "file"
      url: "http://download.ckeditor.com/moono/releases/moono_4.7.1.zip"
    destination: "libraries/ckeditor/skins"

# Optional libraries.

# The following is necessary if you are using bulk uploads.
  plupload:
    directory_name: plupload
    download: "https://github.com/moxiecode/plupload/archive/v1.5.8.zip"
    type: library
    patch:
      # For security, you need to remove the 'examples' directory. This patch automates that
      - "https://www.drupal.org/files/issues/plupload-1_5_8-rm_examples-text-1903850-26.patch"
  backbone:
    type: "library"
    directory_name: "backbone"
    download:
      url: "https://github.com/jashkenas/backbone/archive/1.1.0.zip"
      #url: "http://backbonejs.org/backbone.js"
      type: "file"
  underscore:
    type: "library"
    directory_name: "underscore"
    download:
      type: "file"
      url: "https://github.com/jashkenas/underscore/archive/1.5.2.zip"
      #url: "http://underscorejs.org/underscore-min.js"
  modernizr:
    type: "library"
    directory_name: "modernizr"
    download:
#     url: "https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"
      url: "https://gist.githubusercontent.com/patrickkettner/762017e6f66d8c49027f/raw/8047297efb6196c480ab5b0ad7565eed3327e1d4/modernizr.js"
      type: "file"
  modernizr_min:
    type: "library"
    directory_name: "modernizr"
    download:
      url: "https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"
#      url: "https://gist.githubusercontent.com/patrickkettner/762017e6f66d8c49027f/raw/8047297efb6196c480ab5b0ad7565eed3327e1d4/modernizr.js"
      type: "file"


# Troubleshooting: Following the steps above, you may also want to allow full html, not strip any html tags text formats admin/config/content/formats

# Filter processing order
# 1. (Optional) Ensure that embedded Media tags are not contained in paragraphs
# 2. Convert Media tags to markup
# 3. (Optional) Correct faulty and chopped off HTML
# when troubleshooting, disable the other 'Filters'  
# admin/config/content/formats, making sure that the 'media' ones are enabled
